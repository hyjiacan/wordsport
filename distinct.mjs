import fs from "fs";

function distinct(name) {
  const file = `./public/database/${name}.json`;
  const data = JSON.parse(fs.readFileSync(file, { encoding: "utf-8" }));
  const temp = new Set();
  const uniqueData = [];
  data.forEach((item) => {
    if (temp.has(item.word)) {
      return;
    }
    temp.add(item.word);
    const x = item.phonetic;
    item.phonetic = item.name;
    item.name = x;
    uniqueData.push(item);
  });
  fs.writeFileSync(file, JSON.stringify(uniqueData, null, 2), {
    encoding: "utf-8",
  });
}

// distinct('animals')
distinct("things");
