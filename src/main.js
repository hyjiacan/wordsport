import { createApp } from 'vue'
import App from './App.vue'

const root = window.location.href

function initSpeak() {
  const audioElement = document.createElement('div')
  audioElement.id = 'audio'
  document.body.appendChild(audioElement)

  const script = document.createElement('script')
  script.src = `${root}/speaking/client.js`
  document.head.appendChild(script)
}

function initFont() {
  const style = document.createElement('style')
  const css = document.createTextNode(`@font-face {
    font-family: "Segoe UI";
    src: url(${root}fonts/segoeui.woff), url(${root}fonts/segoeui.woff2);
  }`)
  style.appendChild(css)
  document.head.appendChild(style)
}

// initSpeak()

initFont()

createApp(App).mount('#app')
