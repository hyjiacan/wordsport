import { createApi } from 'unsplash-js'
// import catdata from '../assets/cat.json'

const ACCESS_KEY = 'ov4aCRJtM89EF-_XLVJFLS4cLkb1kC0j2fjtXyUkzl4'

const unsplash = createApi({
  accessKey: ACCESS_KEY,
})


async function get(name) {
  return fetch(`${window.location.href}database/${name}.json`).then((res) => {
    return res.json();
  });
}

export default {
  getManifest() {
    return get("manifest");
  },
  async getData(name) {
    const data = await get(name);
    data.sort((a, b) => {
      return a.word > b.word ? 1 : -1;
    });
    return data;
  },
  async getImage(word) {
    const result = await unsplash.search.getPhotos({
      query: word,
      perPage: 3
    })
    // const result = catdata

    if (result.errors) {
      console.log(response.errors[0])
      return
    }

    const urls = []

    result.response.results.forEach(item => {
      item.urls.description = item.description
      urls.push(item.urls)
    })

    return urls
  }
};
